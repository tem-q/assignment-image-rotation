#include <stdbool.h>
#include <stdio.h>

bool open_file (FILE** file, const char* filename, const char* mode) {
    *file = fopen(filename, mode);
    if (file == NULL) {
        return false;
    }
    return true;
}

bool close_file (FILE* const* file) {
    if (fclose(*file)) {
        return false;
    }
    return true;
}
