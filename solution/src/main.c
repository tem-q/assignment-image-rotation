#include "bmp.h"
#include "file_work.h"
#include "rotate.h"
#include <stdbool.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "Error");
    }

    // read file
    FILE* in = NULL;
    bool open_status = open_file(&in, argv[1], "rb");
    if (!open_status) {
        fprintf(stderr, "Something went wrong with opening the file for reading");
        return 1;
    }

    struct image old = create_img();
    enum read_status read_st = from_bmp(in, &old);
    if (read_st != READ_OK) {
        print_read_err(read_st);
        close_file(&in);
        return 1;
    }


    bool close_status = close_file(&in);
    if (!close_status) {
        fprintf(stderr, "Something went wrong with closing the file for reading");
        return 1;
    }

    // rotate image
    struct image new = rotate(&old);
    free_img(&old);

    // write to file
    FILE* out = NULL;
    open_status = open_file(&out, argv[2], "wb");
    if (!open_status) {
        fprintf(stderr, "Something went wrong with opening the file for writing");
        return 1;
    }

    enum write_status write_st = to_bmp(out, &new);
    if (write_st != WRITE_OK) {
        print_write_err(write_st);
        close_file(&out);
        return 1;
    }
    free_img(&new);

    close_status = close_file(&out);
    if (!close_status) {
        fprintf(stderr, "Something went wrong with closing the file for writing");
        return 1;
    }
    
    return 0;
}
