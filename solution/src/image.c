#include "image.h"
#include <malloc.h>
#include <stdint.h>

struct image create_img () {
    struct image img = {.width = 0, .height = 0, .data = NULL};
    return img;
}

struct image init_img (uint64_t width, uint64_t height) {
    struct image created_image = {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))
    };
    return created_image;
}

void free_img (struct image* img) {
    img->width = 0;
    img->height = 0;
    free(img->data);
    img->data = NULL;
}
