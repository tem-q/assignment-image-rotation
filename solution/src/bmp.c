#include "bmp.h"
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType; // 0x4d42
        uint32_t bfileSize; // File size in bytes
        uint32_t bfReserved; // Reserved data, 0
        uint32_t bOffBits; // The address from which the image begins
        uint32_t biSize; // The size of the header, 40 bytes
        uint32_t biWidth; // The width of the image in pixels
        uint32_t biHeight; // The height of the image in pixels
        uint16_t biPlanes; // 1
        uint16_t biBitCount; // Color depth (number of bits per pixel), 24
        uint32_t biCompression; // Compression method, 0
        uint32_t biSizeImage; // Bitmap size in bytes (without header size)
        uint32_t biXPelsPerMeter; // Horizontal resolution in pixels per meter
        uint32_t biYPelsPerMeter; // Vertical resolution in pixels per meter
        uint32_t biClrUsed; // Number of colors used
        uint32_t biClrImportant; // Number of important colors, 0
};
#pragma pack(pop)


/* for reading */
static enum read_status check_bmp_header (const struct bmp_header header) {
    if (header.bfType != 0x4d42) {
        return READ_INVALID_SIGNATURE;
    } if (header.biSize != 40) {
        return READ_INVALID_HEADER_SIZE;
    } if (header.biPlanes != 1) {
        return READ_INVALID_PLANES;
    } if (header.biBitCount != 24) {
        return READ_INVALID_BIT_COUNT;
    } if (header.biCompression != 0) {
        return READ_INVALID_COMPRESSION;
    } if (header.biClrImportant != 0) {
        return READ_INVALID_IMPORTANT_COLOR;
    }
    return READ_OK;
}

static uint8_t get_padding (uint64_t width) {
    return width % 4;
}

enum read_status from_bmp (FILE* in, struct image* img) {
    struct bmp_header header;
    fread(&header, sizeof(struct bmp_header), 1, in);
    if (check_bmp_header(header) != READ_OK) {
        return check_bmp_header(header);
    }

    *img = init_img(header.biWidth, header.biHeight);
    const uint8_t padding = get_padding(img->width);
    uint64_t count = 0;
    for (size_t row = 0; row < img->height; row++) {
        count = fread(&(img->data[row*(img->width)]), sizeof(struct pixel), img->width, in);
        if (count != img->width) {
            free_img(img);
            return READ_ERROR;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            free_img(img);
            return READ_ERROR;
        }
    }
    return READ_OK;
}


/* for writing */
static struct bmp_header create_bmp_header (const uint64_t width, const uint64_t height) {
    struct bmp_header header = {
        .bfType = 0x4d42,
        .bfileSize = sizeof(struct bmp_header) + width * height * sizeof(struct pixel) + height * get_padding(width),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage =  width * height * sizeof(struct pixel) + height * get_padding(width),
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
    return header;
}

enum write_status to_bmp(FILE* out, const struct image* img) {
    struct bmp_header header = create_bmp_header(img->width, img->height);
    const size_t write_header_status = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (!write_header_status) {
        return WRITE_HEADER_ERROR;
    }
    const uint8_t padding = get_padding(img->width);
    uint64_t padding_value = 0;
    for (size_t row = 0; row < img->height; row++) {
        if (fwrite(&(img->data[row*img->width]), sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        
        if (fwrite(&padding_value, 1, padding, out) != padding) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}


static const char* const read_errors[] = {
    [READ_OK] = "Image is read successfully\n",
    [READ_INVALID_SIGNATURE] = "Incorrect bmp header signature\n",
    [READ_INVALID_HEADER_SIZE] = "Incorrect header size\n",
    [READ_INVALID_PLANES] = "Incorrect number of planes\n",
    [READ_INVALID_BIT_COUNT] = "Incorrect number of bits per pixel\n",
    [READ_INVALID_COMPRESSION] = "Incorrect type of compression\n",
    [READ_INVALID_IMPORTANT_COLOR] = "Incorrect number of important colors\n",
    [READ_ERROR] = "Something went wrong when reading the image\n"
};

static const char* const write_errors[] = {
    [WRITE_OK] = "Image is written successfully\n",
    [WRITE_HEADER_ERROR] = "Something went wrong when writing the header\n",
    [WRITE_ERROR] = "Something went wrong when writing the image\n"
};

void print_read_err (enum read_status err) {
    printf("%s", read_errors[err]);
}

void print_write_err (enum write_status err) {
    printf("%s", write_errors[err]);
}
