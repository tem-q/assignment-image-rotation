#include "rotate.h"
#include <stddef.h>

static struct pixel get_pixel (struct image const* old, const size_t i, const size_t j){
	return old->data[(old->height-j-1)*old->width+i];
}

struct image rotate (const struct image* old) {
    struct image new_image = init_img(old->height, old->width);
    for (size_t i = 0; i < new_image.height; i++) {
        for (size_t j = 0; j < new_image.width; j++) {
            new_image.data[i*new_image.width+j] = get_pixel(old, i, j);
        }
    }
    return new_image;
}
