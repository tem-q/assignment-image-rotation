#pragma once
#include  <stdint.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
  uint64_t width;
  uint64_t height;
  struct pixel* data;
};

struct image create_img ();

struct image init_img (uint64_t width, uint64_t height);

void free_img (struct image* img);
