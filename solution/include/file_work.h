#pragma once
#include <stdbool.h>
#include <stdio.h>

bool open_file (FILE** file, const char* filename, const char* mode);

bool close_file (FILE* const* file);
