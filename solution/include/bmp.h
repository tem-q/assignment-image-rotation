#pragma once
#include "image.h"
#include <stdio.h>

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER_SIZE,
    READ_INVALID_PLANES,
    READ_INVALID_BIT_COUNT,
    READ_INVALID_COMPRESSION,
    READ_INVALID_IMPORTANT_COLOR,
    READ_ERROR
  };

enum read_status from_bmp (FILE* in, struct image* img);

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* out, const struct image* img);

void print_read_err (enum read_status err);

void print_write_err (enum write_status err);
